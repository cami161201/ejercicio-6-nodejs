const { Router } = require('express');
const {materialGet,materialPut,materialPost,materialDelete } = require('../controllers/materialController')

const router = Router();

router.get('/',materialGet);
router.put('/:id',materialPut);
router.post('/',materialPost);
router.delete('/:id',materialDelete);


module.exports = router;